<?php

namespace App\Http\Controllers\backend\subadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Session;
use Hash;

class CommonController extends Controller
{
    public function index(Request $request) {
       if(!empty(Auth::check()) && Auth::user()->role =='2'){
       	   $id = !empty(Auth::user()->id)?Auth::user()->id:'';
           $user = User::find($id);
    	   return view('backend.sub-admin.profile.edit', compact('user'));

       } else  {
            Session::flash('Info','You must be login first');
    		return redirect('/');
       }   
    }

    public function updateprofile(Request $request) {
         if(!empty(Auth::check()) && Auth::user()->role == '2') {
              if($request->all()) {
              	$request->validate([
		              'name' => 'required',
		              'email' => 'required|email|unique:users,email,'.Auth::user()->id,
		              'first_name' => 'required',
		              'last_name' => 'required',
		              'address' => 'required',
		              'city' => 'required',
		              'district' => 'required',
		              'state' => 'required',
		              'country' => 'required',
		              'postal_code' => 'required|min:6|max:6',
		              'about_me' => 'required',
		              'contact_number' => 'required',
               ]);
               $profileArray = [
               	             'name' => $request['name'],
               	             'email' => $request['email'],
               	             'first_name' => $request['first_name'],
               	             'last_name' => $request['last_name'],
               	             'address' => $request['address'],
               	             'country' => $request['country'],
               	             'post_code' => $request['postal_code'],
               	             'bio' => $request['about_me'],
               	             'phone' => $request['contact_number'],
               	             'district' => $request['district'],
               	             'city' => $request['city'],
               	             'state' => $request['state']
                       ];
                $updateprofile = User::where('id','=', Auth::user()->id)->update($profileArray);
                if(!empty($updateprofile)) {
                     Session::flash('success','Profile updated successfully!');
    		         return redirect(route('sub-admin.profile'));
                } else {
                     Session::flash('error','Somerthing went wrong.');
    		         return redirect(route('sub-admin.profile'));
                }
              }
         } else  {
            Session::flash('Info','You must be login first.');
    		return redirect('/');
        } 
    }

    public function changePassword(Request $request) {
    	if(!empty(Auth::check()) && Auth::user()->role == '2') {

             return view('backend.sub-admin.profile.change-password');
    	} else {
    		Session::flash('Info','You must be login first.');
    		return redirect('/');
    	}
    }

    public function updatePassword(Request $request){  
            if(!empty(Auth::check()) && Auth::user()->role =='2') {
             	$request->validate([
		              'current_password' => 'required',
                  'password' => 'required|string|min:8|confirmed',
               ]);
            if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
                // The passwords matches
                Session::flash('error', 'Your current password does not matches with the password you provided. Please try again.');
                    return redirect(route('sub-admin.change-password'));

            }
            if(strcmp($request->get('current_password'), $request->get('password')) == 0){
                //Current password and new password are same
                Session::flash('warning', 'New password should not be same as current password.');
               return redirect(route('sub-admin.change-password'));

            }
            //Change Password
            $user = Auth::user();
            $user->password = bcrypt($request->get('password'));
            $user->save();
            Session::flash('success', 'Password changed successfully !');
            return redirect(route('sub-admin.dashboard'));
        
		    } else {
		    	    Session::flash('Info','You must be login first.');
		    		return redirect('/');
		    }
	}
	
}
