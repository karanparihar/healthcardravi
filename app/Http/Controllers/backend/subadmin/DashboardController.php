<?php

namespace App\Http\Controllers\backend\subadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
   public function index(Request $request) {

    	return view('backend.sub-admin.dashboard');

    }
}
