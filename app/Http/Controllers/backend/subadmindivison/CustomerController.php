<?php

namespace App\Http\Controllers\backend\subadmindivison;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Customers;
use App\Models\FamilyMember;
use Auth;
use Session;
use Route;
use Response;
use Carbon\Carbon;
use Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public function index(Request $request) {
       if(!empty(Auth::check()) && Auth::user()->role =='3'){ 
           return view('backend.sub-admin-divison.customers.index');
       } else  {
            Session::flash('Info','You must be login first');
    		return redirect('/');
       }   
    }
    public function add() {
    	if(!empty(Auth::check()) && Auth::user()->role == '3') {
            return view('backend.sub-admin-divison.customers.add');
    	} else {
    		Session::flash('Info','You must be login first');
    		return redirect('/');
    	}
    }
 
    public function edit($id ='') {
         if(!empty(Auth::check()) && Auth::user()->role =='3') {
              $customer = customers::find($id);
              $members = DB::table('familymember')->where('customer_id',$id)->get();
             
              return view('backend.sub-admin-divison.customers.edit', ['customer' => $customer,'members' => $members]);
         } else {
            Session::flash('Info','You must be login first');
            return redirect('/');
         }
    }
    public function addmember(Request $request,$id ='') {
         if(!empty(Auth::check()) && Auth::user()->role =='3') {
               if($request->ajax())
                 { 
                  $rules = array(
                   'dependent_name'  => 'required',
                   'relation'  => 'required',
                   'gender'  => 'required',
                   'age'  => 'required',
                   'dob'  => 'required',
                   'adhar_number'  => 'required',
                  );
                  $error = Validator::make($request->all(), $rules);
                  if($error->fails())
                  {
                   return response()->json([
                    'error'  => $error->errors()->all()
                   ]);
                  } 
                  $dependent_name = $request->dependent_name;
                  $relation = $request->relation;
                  $gender = $request->gender;
                  $dob = $request->dob;
                  $age = $request->age;
                  $adhar_number = $request->adhar_number;
                  $insert_data =  array();
                 for($count = 0; $count < count($dependent_name); $count++)
                {
                    $data = array(
                        'customer_id' => $id,
                        'relation'  => $relation[$count],
                        'dependent_name' => $dependent_name[$count],
                        'gender'  => $gender[$count],
                        'dob'  => $dob[$count],
                        'age'  => $age[$count],
                        'adhar_number'  => $adhar_number[$count],
                    );
                
                         $insert_data[] = $data; 
                }
                if(!empty($insert_data))
                {
                  DB::table('familymember')->insert($insert_data);
                  return response()->json([
                   'success'  => 'Member Added successfully.'
                  ]);
                } 
            }
         } else {
            Session::flash('Info','You must be login first');
            return redirect('/');
         }
    }
    /*
    echo $ids[$count];
                    if(!empty($ids[$count]))
                    {
                    $whereCondition = ['id' => $ids[$count]];
                     $update_data[] = DB::table('familymember')->where('id',$ids[$count])->update($data);
                     //$member->update($data);
                    //DB::table('familymember')->update($data)where($whereCondition);
                    }else{

*/
                        public function update(Request $request, $id='') {
        if(!empty(Auth::check()) && Auth::user()->role == '3') {
            if(!empty($request->all())) {
               
                $whereCondition = ['id' => $id];
                $userArray = [];
             

                 $userArray = [
                            'category_proof' => $request['category_proof'] , 
                            'rashancard_images' => $request['rashancard_images'] , 
                            'rashancard_number' => $request['rashancard_number'] , 
                            'block' => $request['block'],
                            'panchayat' => $request['panchayat'],
                            'district' => $request['district'], 
                            'village' => $request['village'],
                            'social_group' => $request['social_group'],
                            'state' => $request['state'],
                            'locality_address' => $request['locality_address'],
                            'mobile_number' => $request['mobile_number'],
                       ];
                $updated = Customers::where($whereCondition)->update($userArray);
                if(!empty($updated)) {
                    Session::flash('success','Customer updated successfuly!');
                    return redirect(route('sub-admin-divison.customers'));
                } else {
                   Session::flash('error','Something went wrong.');
                    return redirect(route('sub-admin-divison.customers'));
                }
            }
        } else {
            Session::flash('Info','You must be login first');
            return redirect('/');
        }
    }
    public function store(Request $request) {
       if(!empty(Auth::check()) && Auth::user()->role == '3') {
            if(!empty($request->all())) {
                $userArray = [];
                 $customerArray = [ 
                            'category_proof' => $request['category_proof'] , 
                            'rashancard_images' => $request['rashancard_images'] , 
                            'rashancard_number' => $request['rashancard_number'] , 
                            'block' => $request['block'],
                            'panchayat' => $request['panchayat'],
                            'district' => $request['district'],
                            'parent' => Auth::user()->id,
                            'village' => $request['village'],
                            'social_group' => $request['social_group'],
                            'state' => $request['state'],
                            'locality_address' => $request['locality_address'],
                            'mobile_number' => $request['mobile_number'],
                       ];
                     
                $created = Customers::insert($customerArray);
         
                if(!empty($created)) {
                    Session::flash('success','Customer added successfuly!');
                    return redirect(route('sub-admin-divison.customers'));
                } else {
                   Session::flash('error','Something went wrong.');
                   return redirect(route('sub-admin-divison.customers.add'));
                }
            }
        } else {

            Session::flash('Info','You must be login first');
            return redirect('/');
        }
    }
}
