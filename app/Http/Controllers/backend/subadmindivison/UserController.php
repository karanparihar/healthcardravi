<?php

namespace App\Http\Controllers\backend\subadmindivison;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Session;
use Route;
use Response;
use Carbon\Carbon;
use Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(Request $request) {
       if(!empty(Auth::check()) && Auth::user()->role =='3'){
          
    	   return view('backend.sub-admin-divison.users.index');

       } else  {
            Session::flash('Info','You must be login first');
    		return redirect('/');
       }   
    }
    public function add() {
    	if(!empty(Auth::check()) && Auth::user()->role == '3') {
            return view('backend.sub-admin-divison.users.add');
    	} else {
    		Session::flash('Info','You must be login first');
    		return redirect('/');
    	}
    }

    public function store(Request $request) {
    	if(!empty(Auth::check()) && Auth::user()->role == '3') {
            if(!empty($request->all())) {

            	$userArray = [];
            	$request->validate([
		            'name' => ['required', 'string', 'max:255'],
		            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
		            'role' => ['required'],
		            'state' => ['required'],
		            'district' => ['required'],
		            'password' => ['required', 'string', 'min:8', 'confirmed'],
                    'adhar_number' => 'required|numeric|digits_between:16,16|unique:users',
                    'pan_number' => 'required|string|max:10|min:10|unique:users,pan_number',
                    'image' => 'required|mimes:jpeg,jpg,png|max:5000|min:50',

               ]);
                $adhar_number = str_replace(' ', '', $request['adhar_number']);
                try {
                     if ($request->hasFile('image')) {
                        $image = $request->file('image');
                        $name = time().'.'.$image->getClientOriginalName();
                        $destinationPath = public_path('/user-images');
                        chmod($image->move($destinationPath, $name), 0777);
                        $imageName = 'user-images/'.$name;
                    }
                $userArray = [ 'name' => $request['name'] , 
                            'email' => $request['email'],
                            'role' => $request['role'],
                            'state' => $request['state'],
                            'district' => $request['district'],
                            'parent' => Auth::user()->id,
                            'password' => bcrypt($request['password']),
                            'adhar_number' => $adhar_number,
                            'pan_number' => $request['pan_number'],
                            'image' => $name,
                       ];
                    
                $created = User::insert($userArray);
                if(!empty($created)) {
                    Session::flash('success','User added successfuly!');
                    return redirect(route('sub-admin-divison.users'));
                } 
                }  catch(ModelNotFoundException $err) {
                   Session::flash('error','Something went wrong.');
                   return redirect(route('sub-admin-divison.users'));
                }
            }
    	} else {
    		Session::flash('Info','You must be login first');
    		return redirect('/');
    	}
    }

    public function edit($id ='') {
	     if(!empty(Auth::check()) && Auth::user()->role =='3') {
              $user = User::find($id);
              if(!empty($user)) {
                  if($user['parent'] == Auth::user()->id) {
                     return view('backend.sub-admin-divison.users.edit', compact('user'));
                  } else {
                     Session::flash('warning','Unauthorised action');
                     return redirect(route('sub-admin-divison.users'));
                  }  
              } else {
                  Session::flash('warning','Unauthorised action');
                  return redirect(route('sub-admin-divison.users'));
              }
	     } else {
	      	Session::flash('Info','You must be login first');
			return redirect('/');
	     }
    }

    public function update(Request $request, $id='') {
		if(!empty(Auth::check()) && Auth::user()->role == '3') {
	        if(!empty($request->all())) {
               
                $whereCondition = ['id' => $id];
	        	$userArray = [];
                    $request->validate([
                    'name' => ['required', 'string', 'max:255'],
                    'email' => 'required|email|unique:users,email,'.$id,
                    'state' => ['required'],
                    'district' => ['required'],
                    'adhar_number' => 'required|numeric|digits_between:16,16|unique:users,adhar_number,'.$id,
                    'pan_number' => 'required|string|max:10|min:10|unique:users,pan_number,'.$id,

                 ]);
                try{
                    if ($request->hasFile('image')) {

                        $image = $request->file('image');
                        $name = time().'.'.$image->getClientOriginalName();
                        $destinationPath = public_path('/user-images');
                        chmod($image->move($destinationPath, $name), 0777);
                        if($request['user_image'] !='' && file_exists('user-images/'.$request['user_image'])){
                            $file_path = public_path().'/user-images/'.$request['user_image'];
                            unlink($file_path);
                        }  
                           $imageName = 'user-images/'.$name;
                        } else  {
                            $name = $request['user_image'];
                    }
                 $adhar_number = str_replace(' ', '', $request['adhar_number']);
                 $userArray = [
                            'name' => $request['name'] , 
                            'email' => $request['email'],
                            'state' => $request['state'],
                            'district' => $request['district'],
                            'parent' => Auth::user()->id,
                            'adhar_number' => $adhar_number,
                            'pan_number' => $request['pan_number'],
                            'image' => $name
                       ];
                   
                $updated = User::where($whereCondition)->update($userArray);
                if(!empty($updated)) {
                    Session::flash('success','User updated successfuly!');
                    return redirect(route('sub-admin-divison.users'));
                   }
                } catch(ModelNotFoundException $err) {
                   Session::flash('error','Something went wrong.');
                   return redirect(route('sub-admin-divison.users'));
                }
	        }
		} else {
			Session::flash('Info','You must be login first');
			return redirect('/');
		}
    }

    public function delete(Request $request) {
    	if(!empty(Auth::check()) && Auth::user()->role == '3') {
            if($request->ajax()) {
                $user_id = $request['user_id'];
                $userArray = ['delete_flag' => '1'];
                $whereCondition = ['id' => $request['user_id']];
                $deleted = User::Where($whereCondition)->update($userArray);
                if(!empty($deleted)) {
                        $response = ['success' => 'User deleted successfuly.'];
                        return Response::json($response);
                    } else {
                       $response = ['error' => 'Something went wrong.'];
                       return Response::json($response);
                    }
            }
    	} else {
    		Session::flash('Info','You must be login first');
			return redirect('/');
    	}
    }

    public function changePassword(Request $request) {
        if(!empty(Auth::check()) && Auth::user()->role == '3') {
            if($request->ajax()) {
                
                $rules = array('password' => 'required|min:8|confirmed');
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return Response::json(array('validation_errors' => $validator->getMessageBag()->toArray()), 400);

                } else  {
                     $userArray = [
                                  'password' => bcrypt($request['password'])
                       ];
                $whereCondition = ['id' => $request['user_id']];
                $udpated = User::Where($whereCondition)->update($userArray);
                if(!empty($udpated)) {
                        $response = ['success' => 'password updated successfuly.'];
                        return Response::json($response);
                    } else {
                       $response = ['error' => 'Something went wrong.'];
                       return Response::json($response);
                    }
                }
            }
        } else {
            Session::flash('Info','You must be login first');
            return redirect('/');
        }
    }
}
