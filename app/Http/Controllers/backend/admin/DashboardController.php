<?php

namespace App\Http\Controllers\backend\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Session;

class DashboardController extends Controller
{
    public function index(Request $request) {
      
    	return view('backend.admin.dashboard');

    }
}
