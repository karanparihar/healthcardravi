<?php
use App\Models\Roles;
use App\Models\Districts;
use App\Models\States;
use App\User;
use Auth as Auth;

function getRoles() {
	$roles = Roles::all();
	if(!empty($roles)) {
       return $roles;
	}
}

function getDistricts() {
	$districts = Districts::all();
	if(!empty($districts)) {
       return $districts;
	}
}

function getStates() {
	$states = States::all();
	if(!empty($states)) {
       return $states;
	}
}


 function allUsersByRole($roleId, $parent) {
 	$whereContiton = ['role' =>$roleId, 'delete_flag' => 0 ,'parent' => $parent ];
    $users = User::where($whereContiton)->with('userDistrict','userState')->orderBy('id', 'DESC')->paginate(10);
    if(!empty($users)) {
    	return $users;
    }    
 }

 function allUsersByRoleCount($roleId) {
 	$whereContiton = ['role' =>$roleId, 'delete_flag' => 0 ];
    $users = User::where($whereContiton)->count();
    if(!empty($users)) {
    	return $users;
    } else {
    	$users = 0;
    	return $users;
    } 
 }

 function getAllChildCount($parent, $role) {
 	$whereContiton = ['parent' => $parent ,'role'=> $role];
 	$users = User::where($whereContiton)->count();
 	
 	return $users; 

 }

 function allcustomer() {
  $users = DB::table('customers')->where('parent', Auth::user()->id)->get();
	if(!empty($users)) {
       return $users;
	} 
  }
  function familymember($customer_id) {
    $users = DB::table('familymember')->where('customer_id', $customer_id)->get();
    if(!empty($users)) {
         return $users;
    }
  }

  function getParent($parentId) {
    if(!empty($parentId))
      $user = User::where('parent','=',$parentId)->first();
      return $user['name'];
  }

  function getRoleName($role) {
    if(!empty($role)) {
      $role = Roles::find($role);
      return $role['name'];
    }
  }
