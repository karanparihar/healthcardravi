<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $fillable = ['user_id','block','panchayat','village','rashancard_number','social_group','locality_address','mobile_number','rashancard_images','category_proof',];

}