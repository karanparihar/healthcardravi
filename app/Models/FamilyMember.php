<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FamilyMember extends Model
{
        protected $fillable = ['customer_id','dependent_name','relation','gender','dob','age','adhar_number','adhar_image'];
}
