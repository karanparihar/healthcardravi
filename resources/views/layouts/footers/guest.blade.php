<footer class="footer">
    <div class="container">
        <nav class="float-left">
        <ul>
            <li>
            <a href="http://healthcard.himtechsolution/">
                {{ __('Healthcard') }}
            </a>
            </li>
            <li>
            <a href="http://healthcard.himtechsolution.in/presentation">
                {{ __('About Us') }}
            </a>
            </li>
            <li>
            <a href="http://blog.creative-tim.com">
                {{ __('Blog') }}
            </a>
            </li>
            <li>
            <a href="http://healthcard.himtechsolution.in/license">
                {{ __('Licenses') }}
            </a>
            </li>
        </ul>
        </nav>
        <div class="copyright float-right">
        &copy;
        <script>
            document.write(new Date().getFullYear())
        </script>, made with <i class="material-icons">favorite</i> by
        <a href="http://healthcard.himtechsolution.in/" target="_blank">himtechsolution</a> for a better web.
        </div>
    </div>
</footer>