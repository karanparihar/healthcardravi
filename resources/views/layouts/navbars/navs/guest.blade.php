<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white">
  <div class="container">
    <div class="navbar-wrapper">
      <a class="navbar-brand" href="{{ route('welcome') }}">{{ __('Heathcard') }}</a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
      <span class="sr-only">Toggle navigation</span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
      <span class="navbar-toggler-icon icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end">
      <ul class="navbar-nav">
         @if(Auth::user() && Auth::user()->role == '1')

            <li class="nav-item {{ Route::currentRouteName() == 'welcome'  ? 'active' : '' }}">
              <a href="{{ route('admin.dashboard') }}" class="nav-link">
                <i class="material-icons">face</i> {{ __('Dashboard') }}
              </a>
            </li>
        @endif
         @if(Auth::user() && Auth::user()->role == '2')
            <li class="nav-item {{ Route::currentRouteName() == 'welcome'  ? 'active' : '' }}">
              <a href="{{ route('sub-admin.dashboard') }}" class="nav-link">
                <i class="material-icons">face</i> {{ __('Dashboard') }}
              </a>
            </li>
        @endif
         @if(Auth::user() && Auth::user()->role == '3')
            <li class="nav-item {{ Route::currentRouteName() == 'welcome'  ? 'active' : '' }}">
              <a href="{{ route('sub-admin-divison.dashboard') }}" class="nav-link">
                <i class="material-icons">face</i> {{ __('Dashboard') }}
              </a>
            </li>
        @endif
        @if(Auth::guest())
        <li class="nav-item {{ Route::currentRouteName() == 'login'  ? 'active' : '' }}">
          <a href="{{ route('login') }}" class="nav-link">
            <i class="material-icons">fingerprint</i> {{ __('Login') }}
          </a>
        </li>
        @endif
      </ul>
    </div>
  </div>
</nav>
<!-- End Navbar -->