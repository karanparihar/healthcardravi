@extends('backend.admin.common')

@section('content')
<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Profile</h4>
                  <p class="card-category">Complete your profile</p>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('admin.update-profile')}}">
                    @csrf
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Username</label>
                          <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{Auth::user()->name? Auth::user()->name : old('name')}}" readonly>

                          @error('name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email address</label>
                          <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{Auth::user()->email? Auth::user()->email : old('email')}}" readonly>

                          @error('email')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fist Name</label>
                          <input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{Auth::user()->first_name? Auth::user()->first_name : old('first_name')}}">

                          @error('first_name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Last Name</label>
                          <input type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{Auth::user()->last_name? Auth::user()->last_name : old('last_name')}}">

                          @error('last_name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Adress</label>
                          <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{Auth::user()->address? Auth::user()->address : old('address')}}">

                        @error('address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">District</label>
                          <select name="district" class="form-control @error('district') is-invalid @enderror" >
                            <option value="">Please select one option</option>
                            @php $districts = getDistricts()  @endphp
                            @php $hasDistrict = !empty(Auth::user()->district)?Auth::user()->district:'' @endphp
                            @if(!empty($districts))
                              @foreach($districts as $value)
                               <option value="{{ $value->id }}" {{ $hasDistrict == $value->id ? 'selected' : '' }} {{ $value->state }}>{{ $value->district }}</option>
                              @endforeach
                            @endif
                          </select>

                          @error('district')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">State</label>
                          <select name="state" class="form-control @error('state') is-invalid @enderror">
                            <option value="">Please select one option</option>
                            @php $states = getStates() @endphp
                            @php $hasState = !empty(Auth::user()->state)?Auth::user()->state:'' @endphp
                            @if(!empty($states))
                              @foreach($states as $value)
                               <option value="{{ $value->id }}" {{ $hasState == $value->id ? 'selected' : '' }}>{{ $value->state }}</option>
                              @endforeach
                            @endif
                          </select>

                          @error('state')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">City</label>
                          <input type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{Auth::user()->city? Auth::user()->city : old('city')}}">

                          @error('city')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Country</label>
                          <input type="text" class="form-control @error('country') is-invalid @enderror" name="country" value="{{Auth::user()->country? Auth::user()->country : old('country')}}">

                            @error('country')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Postal Code</label>
                          <input type="text" class="form-control @error('postal_code') is-invalid @enderror" name="postal_code" value="{{Auth::user()->post_code? Auth::user()->post_code : old('postal_code')}}">

                          @error('postal_code')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>About Me</label>
                          <div class="form-group">
                            <label class="bmd-label-floating"> Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo.</label>
                            <textarea class="form-control @error('about_me') is-invalid @enderror" rows="5" name="about_me">{{Auth::user()->bio? Auth::user()->bio : old('about_me')}}</textarea>

                              @error('about_me')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                              @enderror
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Contact Number</label>
                          <input type="text" class="form-control @error('contact_number') is-invalid @enderror" name="contact_number" value="{{Auth::user()->phone? Auth::user()->phone : old('contact_number')}}">
                          @error('contact_number')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Update Profile</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card card-profile">
                <div class="card-avatar">
                  <a href="#pablo" data-toggle="modal" data-target="#myModal" title="Click to edit picture">
                    <img class="img" src="{{asset('backend/assets/img/faces/marc.jpg')}}" />
                  </a>
                </div>
                <div class="card-body">
                  <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                  <h4 class="card-title">{{Auth::user()->first_name}}&nbsp;{{Auth::user()->last_name}}</h4>
                  <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="javascript:void(0)" class="btn btn-primary btn-round" data-toggle="modal" data-target="#profile_picture">Change picture</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="profile_picture" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header ">
              <h5 class="modal-title">Change Profile Picture</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form method="post" id="upload_form" enctype="multipart/form-data">
                 <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Upload picture&nbsp;<i class="text-danger">(jpeg, jpg, png, file size < 5MB)</i></label></label>
                          <input style="opacity: 1 !important;position: initial !important;" type="file" class="form-control" name="profile" required>
                        </div>
                      </div>
                  </div>
              </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Save changes</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
          </div>
        </div>
      </div>
  @endsection