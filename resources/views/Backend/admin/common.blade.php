@include('backend.admin.includes.header')

@yield('content')

@include('backend.admin.includes.footer')