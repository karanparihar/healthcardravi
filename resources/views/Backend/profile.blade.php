@extends('Backend/common')

@section('content')
<style>
.card-header {
    padding: 15px;
    border-bottom: 1px solid #e5e5e5;
    background: linear-gradient(60deg, #ab47bc, #8e24aa);
    color:#fff;
}
.dd { 
     width: 100%;
     height: auto;
     vertical-align: middle;
     border-style: none;
   }
.fileContainer{
    padding: 10px;
    background: linear-gradient(60deg, #ab47bc, #8e24aa); 
    display: table;
    color: #fff;
    border: 1px solid #e5e5e5;
    margin: auto;
    width: 60%;
    text-align: center;
     }
input[type="file"] {
    display: none;
}
</style>
<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Profile</h4>
                  <p class="card-category">Complete your profile</p>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('admin.update')}}">
                    @csrf
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Username</label>
                          <input type="text" class="form-control" name="name" value="{{Auth::user()->name? Auth::user()->name : old('name')}}">
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email address</label>
                          <input type="email" class="form-control" name="email" value="{{Auth::user()->email? Auth::user()->email : old('email')}}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fist Name</label>
                          <input type="text" class="form-control" name="first_name" value="{{Auth::user()->first_name? Auth::user()->first_name : old('first_name')}}">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Last Name</label>
                          <input type="text" class="form-control" name="last_name" value="{{Auth::user()->last_name? Auth::user()->last_name : old('last_name')}}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Adress</label>
                          <input type="text" class="form-control" name="address" value="{{Auth::user()->address? Auth::user()->address : old('address')}}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">City</label>
                          <input type="text" class="form-control" name="city" value="{{Auth::user()->city? Auth::user()->city : old('city')}}">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Country</label>
                          <input type="text" class="form-control" name="country" value="{{Auth::user()->country? Auth::user()->country : old('country')}}">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Postal Code</label>
                          <input type="text" class="form-control" name="postal_code" value="{{Auth::user()->postal_code? Auth::user()->postal_code : old('postal_code')}}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>About Me</label>
                          <div class="form-group">
                            <label class="bmd-label-floating"> Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo.</label>
                            <textarea class="form-control" rows="5" name="about_me">{{Auth::user()->about_me? Auth::user()->about_me : old('about_me')}}</textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Contact Number</label>
                          <input type="text" class="form-control" name="contact_number" value="{{Auth::user()->contact_number? Auth::user()->contact_number : old('contact_number')}}">
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Update Profile</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card card-profile">
                <div class="card-avatar">
                  <a href="#pablo" data-toggle="modal" data-target="#myModal" title="Click to edit picture">
                    <img class="img" src="{{asset('backend/assets/img/faces/marc.jpg')}}" />
                  </a>
                </div>
                <div class="card-body">
                  <h6 class="card-category text-gray">CEO / Co-Founder</h6>
                  <h4 class="card-title">{{Auth::user()->first_name}}&nbsp;{{Auth::user()->last_name}}</h4>
                  <p class="card-description">
                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                  </p>
                  <a href="#pablo" class="btn btn-primary btn-round">Follow</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="card-header card-header-primary">
          <h4 class="modal-title">Change Profile Picture</h4>
        </div>
        <div class="modal-body">

          <img class="img dd"id="profile-picture1" src="{{asset('backend/assets/img/faces/marc.jpg')}}" />
                 
        </div>
          <form action="{{ route('admin.store-image') }}" enctype="multipart/form-data" method="POST">

          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <label class="fileContainer">
           Click to upload the file!
          <input type="file" name="image" id="profile-picture">
          </label>
        <div class="modal-footer">
          <button type="submit" id="upload" class="btn btn-primary btn-round upload-image" >Upload</button>
        </form>
          <button type="button" class="btn btn-primary btn-round" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
 @endsection