@extends('backend.sub-admin.common')

@section('content')
<div class="content">
        <div class="container-fluid">
           <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
              <a href="{{route('sub-admin.users')}}" class="btn btn-primary">
                Back
              </a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Add User</h4>
                  <p class="card-category">Complete the form</p>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('sub-admin.users.store')}}">
                    @csrf
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Full name</label>
                          <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{old('name')}}"  required>
                         @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>
                     <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email</label>
                          <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{old('email')}}"  required autocomplete="off">
                          @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>
                    
                     <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Role</label>
                          <select name="role" class="form-control @error('role') is-invalid @enderror" required>
                          	<option value="">Please select one option</option>
                          	@php $roles = getRoles() @endphp
                          	@if(!empty($roles))
                          	  @foreach($roles as $value)
                               @if($value->id == 1 || $value->id ==2)
                               @php continue; @endphp
                               @endif
                          	   <option value="{{ $value->id }}">{{ $value->name }}</option>
                          	  @endforeach
                          	@endif
                          </select>
                           @error('role')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>

                     <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">State</label>
                          <select name="state" class="form-control @error('state') is-invalid @enderror" required>
                          	<option value="">Please select one option</option>
                            @php $states = getStates() @endphp
                          	@if(!empty($states))
                          	  @foreach($states as $value)
                          	   <option value="{{ $value->id }}">{{ $value->state }}</option>
                          	  @endforeach
                          	@endif
                          </select>
                          @error('state')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    

                     
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">District</label>
                          <select name="district" class="form-control @error('district') is-invalid @enderror" required>
                          	<option value="">Please select one option</option>
                            @php $districts = getDistricts()  @endphp
                          	@if(!empty($districts))
                          	  @foreach($districts as $value)
                          	   <option value="{{ $value->id }}">{{ $value->district }}</option>
                          	  @endforeach
                          	@endif
                          </select>
                          @error('district')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>

                     <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Password</label>
                          <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"   required autocomplete="off">
                           @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Confirm Password</label>
                          <input type="password" class="form-control" name="password_confirmation"  required autocomplete="off">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Adhar Card Number</label>
                          <input type="text" class="form-control @error('adhar_number') is-invalid @enderror" name="adhar_number" value="{{old('adhar_number')}}" id="adhar_card" maxlength="16" minlength="16" required>
                         @error('adhar_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>

                      <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Pan Card Number</label>
                          <input type="text" class="form-control @error('pan_number') is-invalid @enderror" name="pan_number" value="{{old('pan_number')}}"  required>
                         @error('pan_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Image&nbsp;(<span class="text-danger">format: jpg, jpeg, png, Size: > 50KB & < 5MB</span>)</label>
                          <input id="chooseFile" type="file" class="form-control @error('image') is-invalid @enderror" name="image"  style="opacity: 1 !important;position: unset!important;" required>
                          <img style="height:150px !important;width:300px !important;" id="show_image" src="#" alt="image preview"></br>
                         @error('image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>
                    
                   
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
         
          </div>
        </div>
      </div>
  @endsection