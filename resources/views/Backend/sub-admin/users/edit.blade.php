@extends('backend.sub-admin.common')

@section('content')
<div class="content">
        <div class="container-fluid">
           <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
              <a href="{{route('sub-admin.users')}}" class="btn btn-primary">
                Back
              </a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit User</h4>
                  <p class="card-category">Complete the form</p>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('sub-admin.users.update', $user['id'])}}">
                    @csrf
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Full name</label>
                          <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{old('name', $user['name'])}}"  required>
                         @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>
                     <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email</label>
                          <input type="email" class="form-control" name="email" value="{{old('email', $user['email'])}}"  required autocomplete="off">
                          @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>
                    
                     <!-- <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Role</label>
                          <select name="role" class="form-control" required>
                          	<option value="">Please select one option</option>
                          	@php $roles = getRoles() @endphp
                          	@if(!empty($roles))
                          	  @foreach($roles as $value)

                          	   <option value="{{ $value->id }}" {{ $user['role'] == $value->id ? 'selected' : '' }}>{{ $value->name }}</option>
                          	  @endforeach
                          	@endif
                          </select>
                           @error('role')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div -->

                     <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">State</label>
                          <select name="state" class="form-control" required>
                          	<option value="">Please select one option</option>
                            @php $states = getStates() @endphp
                          	@if(!empty($states))
                          	  @foreach($states as $value)
                          	   <option value="{{ $value->id }}" {{ $user['state'] == $value->id ? 'selected' : '' }}>{{ $value->state }}</option>
                          	  @endforeach
                          	@endif
                          </select>
                          @error('state')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">District</label>
                          <select name="district" class="form-control" required>
                          	<option value="">Please select one option</option>
                            @php $districts = getDistricts()  @endphp
                          	@if(!empty($districts))
                          	  @foreach($districts as $value)
                          	   <option value="{{ $value->id }}" {{ $user['district'] == $value->id ? 'selected' : '' }}>{{ $value->district }}</option>
                          	  @endforeach
                          	@endif
                          </select>
                          @error('district')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>

                    
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Adhar Card Number</label>
                          <input type="text" class="form-control @error('adhar_number') is-invalid @enderror" name="adhar_number" value="{{old('adhar_number', $user['adhar_number'])}}" id="adhar_card" maxlength="16" minlength="16" required>
                         @error('adhar_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>

                      <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Pan Card Number</label>
                          <input type="text" class="form-control @error('pan_number') is-invalid @enderror" name="pan_number" value="{{old('pan_number', $user['pan_number'])}}"  required>
                         @error('pan_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>
                    
                       <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Image&nbsp;(<span class="text-danger">format: jpg, jpeg, png, Size: > 50KB & < 5MB</span>)</label>
                          <input id="chooseFile" type="file" class="form-control @error('image') is-invalid @enderror" name="image"  style="opacity: 1 !important;position: unset!important;" >
                          <input type="hidden" name="user_image" value="{{$user['image']}}">
                          <img style="height:150px !important;width:300px !important;" id="show_image" src="{{asset('user-images/'.$user['image'])}}" alt="image preview"></br>
                           @if(!empty($user['image']) && file_exists('user-images/'.$user['image'])) 
                               @php $image = $user['image'] @endphp
                            @else
                               @php $image = 'images.png' @endphp
                            @endif
                           <a data-caption="caption here" href="{{ asset('user-images/'.$image) }}" data-lightbox="roadtrip">
                            <img style="height:150px !important;width:300px !important;" id="user_image" src="{{asset('user-images/'.$image)}}" alt="image preview"></a></br>
                         @error('image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                    </div>
                 
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
         
          </div>
        </div>
      </div>
  @endsection