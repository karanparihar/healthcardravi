@extends('backend.sub-admin.common')

@section('content')

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-3 col-md-6 col-sm-6">
				<a href="{{route('sub-admin.users.add')}}" class="btn btn-primary">
					Add User
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header card-header-primary">
						<h4 class="card-title ">Sub Division Officer's</h4>
						<p class="card-category">List of all users sub divison officer</p>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							@php $users = allUsersByRole('3',Auth::user()->id) @endphp
							@if(count($users) > 0)
							<table class="table">
								<thead class=" text-primary">
									<th>S.No</th>
									<th>Name</th>
									<th>Email</th>
									<th>District</th>
									<th>Sub Divison Officer's Added</th>
									<th>Image</th>
									<th>Added By</th>
									<th>Action</th>
									</thead>
									<tbody>
										@php $i = 0; $j = 0; $k = 0; $l =0;
										     $j1 = 0; $k1 = 0; $l1 =0; 
										 @endphp
										@foreach($users as $value)
										<tr>
											<td>
												{{ ++$i }} 
											</td>
											<td>
												{{ $value->name }}
											</td>
											<td class="text-primary">
												{{ $value->email }}
											</td>
											<td>
												{{ $value->userDistrict['district'] }}
											</td>
										
											@php $count = getAllChildCount($value->id, 3) @endphp
											@if(!empty($count))
											<td class="text-success text-center">
											    {{ $count  }}
											</td>
											@else 
											 <td class="text-danger text-center">
											 	{{ $count }}
											 </td>
											@endif
											<td>
										     
										        @if(!empty($value->image) && file_exists('user-images/'.$value->image)) 
										           @php $image = $value->image @endphp
										        @else
										           @php $image = 'images.png' @endphp
										        @endif
										    
												<a data-caption="caption here" href="{{ asset('user-images/'.$image) }}" data-lightbox="roadtrip"><img src="{{ asset('user-images/'.$image) }}" alt="image" style="height: 50px;width:80px;"></a>
											</td>
											<td>{{ getParent($value->parent) }}</td>
											 <td class="td-actions text-center">
					                            <a href="{{route('sub-admin.users.edit', $value->id)}}"  rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
					                                <i class="material-icons">edit</i>
					                            </a>
					                            <button type="button" rel="tooltip" title="Change password" class="btn btn-warning btn-link btn-sm" data-toggle="modal" data-target="#changePassword{{ $j++ }}">
					                                <i class="material-icons">lock</i>
					                            </button>
					                            <button type="button" rel="tooltip" title="View Detail" class="btn btn-success btn-link btn-sm" data-toggle="modal" data-target="#Viewdetail{{ $k++  }}">
					                                <i class="material-icons">visibility</i>
					                            </button>
					                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm" data-toggle="modal" data-target="#deleteuserModal{{ $l++ }}">
					                                <i class="material-icons">close</i>
					                            </button>
				                            </td>
											
										</tr>
										<div class="modal fade" id="changePassword{{ $j1++ }}" tabindex="-1" role="dialog">
										  <div class="modal-dialog" role="document">
										    <div class="modal-content">
										      <div class="modal-header ">
										        <h5 class="modal-title">Change Password for <b class="">{{$value->name}}</b></h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										      <div class="modal-body">
										      	<span id="password_error{{$value->id}}" class="text-danger"></span>
										        <form>
							                     <div class="row">
							                      <div class="col-md-12">
							                        <div class="form-group">
							                          <label class="bmd-label-floating">Password</label>
							                          <input type="password" class="form-control" id="password{{$value->id}}" name="password"   required autocomplete="off">
							                          
							                        </div>
							                      </div>
							                    </div>

							                    <div class="row">
							                      <div class="col-md-12">
							                        <div class="form-group">
							                          <label class="bmd-label-floating">Confirm Password</label>
							                          <input type="password" class="form-control" name="password_confirmation"  id="password_confirmation{{$value->id}}" required autocomplete="off">
							                          
							                        </div>
							                      </div>
							                    </div>
										      </div>

										      <div class="modal-footer">
										        <button type="button" class="btn btn-warning" onclick= "changeUserPassword('{{$value->id}}')">Change password</button>
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										      </div>
										     </form>
										    </div>
										  </div>
										</div>

										<div class="modal fade" id="deleteuserModal{{ $l1++ }}" tabindex="-1" role="dialog">
										  <div class="modal-dialog" role="document">
										    <div class="modal-content">
										      <div class="modal-header ">
										        <h5 class="modal-title">Delete User <b>{{ $value->name }}</b></h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										      <div class="modal-body">
										        <p>Are you sure you want to delete the user.
										        <span class="text-danger">This process can't be undone.</span>
										      </div>
										      <div class="modal-footer">
										        <button type="button" class="btn btn-danger" onclick= "deleteuser('{{$value->id}}')">Delete</button>
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										      </div>
										    </div>
										  </div>
										</div>

										<div class="modal fade bd-example-modal-lg" id="Viewdetail{{ $k1++ }}" tabindex="-1" role="dialog">
										  <div class="modal-dialog" role="document">
										    <div class="modal-content">
										      <div class="modal-header ">
										       <h5 class="modal-title">Sub Divison officer Detail&nbsp;-&nbsp; <b>{{$value->name}}</b></h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										       <div class="modal-body" style="overflow-y: scroll; height:500px;">
										       
										        <div class="card card-profile ml-auto mr-auto" style="max-width: 360px">
												  <div class="card-header card-header-image">
													 @if(!empty($value->image) && file_exists('user-images/'.$value->image)) 
												           @php $image = $value->image @endphp
											         @else
											           @php $image = 'images.png' @endphp
											          @endif
												      <a href="#pablo">
												            <img class="img" src="{{ asset('user-images/'.$image) }}" style="height:175px;width: 330px;">
												        </a>
												    </div>
												    <div class="card-body ">
												        <h4 class="card-title">{{$value->name}}</h4>
												        <h6 class="card-category text-primary">{{ getRoleName($value->role) }}</h6>
												    </div>
												    <div class="card-body ">
												        <h4 class="card-title">Added By</h4>
												        <h6 class="card-category text-primary">{{ getParent($value->parent) }}</h6>
												    </div>
                                                    
                                                     <div class="card-body ">
												        <h4 class="card-title">Email</h4>
												        <h6 class="card-category text-primary">{{ $value->email }}</h6>
												    </div>

                                                      <div class="card-body ">
												        <h4 class="card-title">Phone Number</h4>
												        <h6 class="card-category text-primary">{{ $value->phone }}</h6>
												    </div>

												     <div class="card-body ">
												        <h4 class="card-title">Adhar Number</h4>
												        <h6 class="card-category text-primary">{{ $value->adhar_number }}</h6>
												    </div>

												     <div class="card-body ">
												        <h4 class="card-title">Pan Card</h4>
												        <h6 class="card-category text-primary">{{ $value->pan_number }}</h6>
												    </div>

												     <div class="card-body ">
												        <h4 class="card-title">District</h4>
												        <h6 class="card-category text-primary">{{ $value->userDistrict['district'] }}</h6>
												    </div>

												    <div class="card-body ">
												        <h4 class="card-title">Address</h4>
												        <h6 class="card-category text-primary">{{ $value->address }}</h6>
												    </div>
												    <div class="card-body ">
												        <h4 class="card-title">State</h4>
												        <h6 class="card-category text-primary">{{ $value->userState['state'] }}</h6>
												    </div>
												     <div class="card-body ">
												        <h4 class="card-title">District</h4>
												        <h6 class="card-category text-primary">{{ $value->userDistrict['district'] }}</h6>
												    </div>

                                                      <div class="card-body ">
												        <h5 class="card-title">City</h4>
												        <h6 class="card-category text-primary">{{ $value->city }}</h6>
												    </div>
												  
												</div>

										      </div>
										      <div class="modal-footer">
										        
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										      </div>
										    </div>
										  </div>
										</div>

										@endforeach
									</tbody>
								</table>
								<div class="custom-pagination text-center">
					               {!! $users->render() !!}
					            </div>
								@else
									<h3 class="text-center text-danger">No records available.</h3>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>

               
            <div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Customers</h4>
							<p class="card-category">List of all customers</p>
						</div>
						<div class="card-body">
							<div class="table-responsive">
							   @php $users = allUsersByRole('4', Auth::user()->id) @endphp
							   @if(count($users) > 0)
								<table class="table">
									<thead class=" text-primary">
										<th>S.No</th>
										<th>Name</th>
										<th>Email</th>
										<th>District</th>
										<th>Image</th>
										<th>Added By</th>
										<th>Action</th>
										</thead>
										<tbody>
										@php $i = 0; $k = 0; $k1 = 0; @endphp
										@foreach($users as $value)
											<tr>
												<td>
													{{ ++$i }} 
												</td>
												<td>
													{{ $value->name }}
												</td>
												<td class="text-primary">
													{{ $value->email }}
												</td>
												<td>
													{{ $value->userDistrict['district'] }}
												</td>
												<td>
										     
										        @if(!empty($value->image) && file_exists('user-images/'.$value->image)) 
										           @php $image = $value->image @endphp
										        @else
										           @php $image = 'images.png' @endphp
										        @endif
										    
												<a data-caption="caption here" href="{{ asset('user-images/'.$image) }}" data-lightbox="roadtrip"><img src="{{ asset('user-images/'.$image) }}" alt="image" style="height: 50px;width:80px;"></a>
											    </td>
											    <td>{{ getParent($value->parent) }}</td>
												<td class="text-primary">
												<button type="button" rel="tooltip" title="View Detail" class="btn btn-success btn-link btn-sm" data-toggle="modal" data-target="#Viewdetail-customer{{ $k++  }}">
					                                <i class="material-icons">visibility</i>
					                            </button>
												</td>
												
											</tr>
										<div class="modal fade bd-example-modal-lg" id="Viewdetail-customer{{ $k1++ }}" tabindex="-1" role="dialog">
										  <div class="modal-dialog" role="document">
										    <div class="modal-content">
										      <div class="modal-header ">
										        <h5 class="modal-title">Sub Divison officer Detail&nbsp;-&nbsp; <b>{{$value->name}}</b></h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										      <div class="modal-body" style="overflow-y: scroll; height:500px;">
										       
										        <div class="card card-profile ml-auto mr-auto" style="max-width: 360px">
												  <div class="card-header card-header-image">
													 @if(!empty($value->image) && file_exists('user-images/'.$value->image)) 
												           @php $image = $value->image @endphp
											         @else
											           @php $image = 'images.png' @endphp
											          @endif
												      <a href="#pablo">
												            <img class="img" src="{{ asset('user-images/'.$image) }}" style="height:175px;width: 330px;">
												        </a>
												    </div>

												    <div class="card-body ">
												        <h4 class="card-title">{{$value->name}}</h4>
												        <h6 class="card-category text-primary">{{ getRoleName($value->role) }}</h6>
												    </div>
												    <div class="card-body ">
												        <h4 class="card-title">Added By</h4>
												        <h6 class="card-category text-primary">{{ getParent($value->parent) }}</h6>
												    </div>
                                                    
                                                     <div class="card-body ">
												        <h4 class="card-title">Email</h4>
												        <h6 class="card-category text-primary">{{ $value->email }}</h6>
												    </div>

                                                      <div class="card-body ">
												        <h4 class="card-title">Phone Number</h4>
												        <h6 class="card-category text-primary">{{ $value->phone }}</h6>
												    </div>

												     <div class="card-body ">
												        <h4 class="card-title">Adhar Number</h4>
												        <h6 class="card-category text-primary">{{ $value->adhar_number }}</h6>
												    </div>

												     <div class="card-body ">
												        <h4 class="card-title">Pan Card</h4>
												        <h6 class="card-category text-primary">{{ $value->pan_number }}</h6>
												    </div>

												     <div class="card-body ">
												        <h4 class="card-title">District</h4>
												        <h6 class="card-category text-primary">{{ $value->userDistrict['district'] }}</h6>
												    </div>

												    <div class="card-body ">
												        <h4 class="card-title">Address</h4>
												        <h6 class="card-category text-primary">{{ $value->address }}</h6>
												    </div>
												    <div class="card-body ">
												        <h4 class="card-title">State</h4>
												        <h6 class="card-category text-primary">{{ $value->userState['state'] }}</h6>
												    </div>
												     <div class="card-body ">
												        <h4 class="card-title">District</h4>
												        <h6 class="card-category text-primary">{{ $value->userDistrict['district'] }}</h6>
												    </div>

                                                      <div class="card-body ">
												        <h5 class="card-title">City</h4>
												        <h6 class="card-category text-primary">{{ $value->city }}</h6>
												    </div>
												  
												</div>

										      </div>
										      <div class="modal-footer">
										        
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										      </div>
										    </div>
										  </div>
										</div>
										@endforeach
											
										</tbody>
									</table>
									<div class="custom-pagination text-center">
					                    {!! $users->render() !!}
					                </div>
									@else
                                      <h3 class="text-center text-danger">No record available.</h3>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		@endsection
