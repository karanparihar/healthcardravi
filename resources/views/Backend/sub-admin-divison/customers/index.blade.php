@extends('backend/sub-admin-divison/common')

@section('content')

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-3 col-md-6 col-sm-6">
				<a href="{{route('sub-admin-divison.customers.add')}}" class="btn btn-primary">
					Add Customer
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header card-header-primary">
						<h4 class="card-title ">Customers</h4>
						<p class="card-category">List of all users divison officer</p>
					</div>
					<div class="card-body">
						<div class="table-responsive"> 

							@php $users = allcustomer() @endphp
							@if(count($users) > 0)
							<table class="table">
								<thead class=" text-primary">
									<th>S.No</th> 
									<th>Rashancard Number</th>
									<th>Social Group</th>
									<th>Action</th>
									</thead>
									<tbody>
										@php $states = getStates() @endphp
										@php $districts = getDistricts()  @endphp

										@php $i = 0; $j = 0; $k = 0; @endphp
										@foreach($users as $value)
										<tr>
											<td>
												{{ ++$i }} 
											</td> 
											<td>
												{{ $value->rashancard_number }}
											</td>
											<td>
												{{ $value->social_group }}
											</td>
											 <td class="td-actions text-center">
					                            <a href="{{route('sub-admin-divison.customers.edit', $value->id)}}"  rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
					                                <i class="material-icons">edit</i>
					                            </a>
					                         
				                            </td>
											
										</tr> 
										@endforeach
									</tbody>
								</table>
								@else
								
									<h3 class="text-center text-danger">No records available.</h3>
								
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
 
			</div>
		</div>
		@endsection
