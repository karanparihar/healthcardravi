@extends('backend/sub-admin-divison/common')
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
 
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

@section('content')
<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Customer</h4>
                  <p class="card-category">Complete the form</p>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('sub-admin-divison.customers.update', $customer['id'])}}">
                    @csrf
                    <div class="row">
                      <div class="col-md-6">
                       <div class="form-group">
                            <label class="bmd-label-floating">State</label>
                            <select name="state" class="form-control" required>
                              <option value="">Please select one option</option>
                              @php $states = getStates() @endphp
                              @if(!empty($states))
                                @foreach($states as $value)
                                 <option value="{{ $value->id }}" {{ $customer['state'] == $value->id ? 'selected' : '' }}>{{ $value->state }}</option>
                                @endforeach
                              @endif
                            </select>
                            @error('state')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                            @enderror
                          </div>
                      </div> 
<div class="col-md-6">
                   <div class="form-group">
                          <label class="bmd-label-floating">District</label>
                          <select name="district" class="form-control" required>
                            <option value="">Please select one option</option>
                            @php $districts = getDistricts()  @endphp
                            @if(!empty($districts))
                              @foreach($districts as $value)
                               <option value="{{ $value->id }}" {{ $customer['district'] == $value->id ? 'selected' : '' }}>{{ $value->district }}</option>
                              @endforeach
                            @endif
                          </select>
                          @error('district')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>                      
                    </div>
                  <div class="col-md-6">
                     <div class="form-group">
                          <label class="bmd-label-floating">Block</label>
                          <input type="text" class="form-control" name="block" value="{{old('block', $customer['block'])}}"  required autocomplete="off">
                          @error('block')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                    </div> 
 

                  <div class="col-md-6">
                     <div class="form-group">
                          <label class="bmd-label-floating">Panchayat</label>
                          <input type="text" class="form-control" name="panchayat" value="{{old('panchayat', $customer['panchayat'])}}"  required autocomplete="off">
                          @error('panchayat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                    </div> 
 
                  <div class="col-md-6">
                     <div class="form-group">
                          <label class="bmd-label-floating">Village</label>
                          <input type="text" class="form-control" name="village" value="{{old('village', $customer['village'])}}"  required autocomplete="off">
                          @error('village')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                    </div> 
                  <div class="col-md-6">
                     <div class="form-group">
                          <label class="bmd-label-floating">Social Group</label>
                          <input type="text" class="form-control" name="social_group" value="{{old('social_group', $customer['social_group'])}}"  required autocomplete="off">
                          @error('social_group')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                    </div> 
 
                  <div class="col-md-6">
                     <div class="form-group">
                          <label class="bmd-label-floating">Address</label>
                          <input type="text" class="form-control" name="locality_address" value="{{old('locality_address', $customer['locality_address'])}}"  required autocomplete="off">
                          @error('locality_address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                    </div> 
 
                  <div class="col-md-6">
                     <div class="form-group">
                          <label class="bmd-label-floating">Mobile Number</label>
                          <input type="text" class="form-control" name="mobile_number" value="{{old('mobile_number', $customer['mobile_number'])}}"  required autocomplete="off">
                          @error('mobile_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div> 
                  </div>
 
                  <div class="col-md-6">
                     <div class="form-group">
                          <label class="bmd-label-floating">Rashan Card</label>
                          <input type="text" class="form-control" name="rashancard_number" value="{{old('rashancard_number', $customer['rashancard_number'])}}"  required autocomplete="off">
                          @error('rashancard_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div> 
                  </div>
 
                    </div>
                      
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>           


                 <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Add Family Members</h4>
                  <p class="card-category">Complete the form</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
  <table class="table table-bordered table-striped" id="edit_table">
               <thead>
                <tr>
                    <th>Dependent Name</th>
                    <th>Relation</th>
                    <th>Gender</th>
                    <th>Dob</th>
                    <th>Age</th>
                    <th>Adhar Number</th>
                    <th>Action</th>
                </tr>
               </thead>
               <tbody> 
                          @foreach($members as $key => $value)
                             <tr>
                              <td>
                             <input type="hidden" value="{{$value->id}}" name="id[]"   class="form-control name_list"/>
                             <input type="text" value="{{$value->dependent_name}}" name="dependent_name[]" placeholder="Enter Dependent Name"  class="form-control name_list" required/>
                             </td>
                             <td>
                              <select name="relation[]" class="form-control" required><option>Select</option><option value="Father" {{'Father' == $value->relation ? 'selected' : '' }}>Father</option><option value="Mother" {{'Mother' == $value->relation ? 'selected' : '' }}>Mother</option><option value="Son" {{'Son' == $value->relation ? 'selected' : '' }}>Son</option><option value="Daughter" {{'Daughter' == $value->relation ? 'selected' : '' }}>Daughter</option></select>
                             </td>
                             <td><select name="gender[]" class="form-control" required><option value="">Select</option><option value="Female" {{'Female' == $value->gender ? 'selected' : '' }}>Female</option><option value="Male" {{'Male' == $value->gender ? 'selected' : '' }}>Male</option></select>
                              </td><td><input type="date" name="dob[]" value="{{$value->dob}}"  placeholder="Enter Dob" class="form-control name_list" required/>
                              </td><td>
                              <input type="text" value="{{$value->age}}"  name="age[]" placeholder="Enter Age" class="form-control name_list" required /></td><td><input type="text" name="adhar_number[]" value="{{$value->adhar_number}}"  placeholder="Enter Adhar" class="form-control name_list" required/>
                              </td><td>
                               <i class="material-icons class="btn btn-success btn-lg" data-toggle="modal" data-target="#modalForm{{$key}}"">edit</i> <i class="remove material-icons">delete</i>
<div class="modal fade" id="modalForm{{$key}}" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Member Form</h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form method="POST" id="modalFormId{{$key}}" action="{{route('sub-admin-divison.customers.update', $customer['id'])}}">
                  @csrf
                <!-- <form role="form" id="modalFormId{{$key}}"> -->
                    <div class="form-group">
                        <label for="inputName">Name</label>
                         <input type="hidden" value="{{$value->id}}" name="id[]"   class="form-control name_list"/>
                             <input type="text" value="{{$value->dependent_name}}" name="dependent_name[]" placeholder="Enter Dependent Name"  class="form-control name_list" required/>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">Relation</label>
                        <select name="relation[]" class="form-control" required><option>Select</option><option value="Father" {{'Father' == $value->relation ? 'selected' : '' }}>Father</option><option value="Mother" {{'Mother' == $value->relation ? 'selected' : '' }}>Mother</option><option value="Son" {{'Son' == $value->relation ? 'selected' : '' }}>Son</option><option value="Daughter" {{'Daughter' == $value->relation ? 'selected' : '' }}>Daughter</option></select>
                    </div>
                    <div class="form-group">
                        <label for="inputMessage">Gender</label>
                        <select name="gender[]" class="form-control" required><option value="">Select</option><option value="Female" {{'Female' == $value->gender ? 'selected' : '' }}>Female</option><option value="Male" {{'Male' == $value->gender ? 'selected' : '' }}>Male</option></select>
                    </div>  <div class="form-group">
                        <label for="inputMessage">Age</label>
                        <input type="text" value="{{$value->age}}"  name="age[]" placeholder="Enter Age" class="form-control name_list" required /> 
                    </div>  <div class="form-group">
                        <label for="inputMessage">Adhar Card Number</label>
                        <input type="text" name="adhar_number[]" value="{{$value->adhar_number}}"  placeholder="Enter Adhar" class="form-control name_list" required/>
                    </div>  
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button id="update_me" modal_form_id="modalFormId{{$key}}"  type="submit" class="btn btn-primary pull-right updatemodaldata">Submit</button>

                <!-- <button type="button" id="submitContactForm" class="btn btn-primary submitBtn">SUBMIT</button> -->
            </div>

        </div>
    </div>
</div>
 </form>
 
                             </td></tr>
                          @endforeach 
                          
               </tbody>
           </table>  
                <form method="post" id="dynamic_form">
                 <span id="result"></span>
                  
                 <table class="table table-bordered table-striped" id="user_table">
               
               <tbody> 
                   
               </tbody>
           </table> 
            @csrf
                  <button type="button" name="add" id="add" class="btn btn-success">Add</button>
                  <input type="submit" name="save" id="save" class="btn btn-primary" value="Save" />
                </form>
   </div>
                </div>
              </div>

            </div>
         
          </div>
        </div>
      </div>




<script>
$(document).ready(function(){

  $(".updatemodaldata").on("click", function(e) {
    e.preventDefault();
       var form_id= $(this).attr('modal_form_id');
        var values = $("#"+form_id).serialize();
console.log(form_id);
console.log(values);
     $.ajax({
            url:'{{ route("sub-admin-divison.customers.addmember", $customer['id']) }}',
            method:'post',
            data: $("#"+form_id).serialize();
            dataType:'json',
            beforeSend:function(){
               // $('#save').attr('disabled','disabled');
            },
            success:function(data)
            {
                if(data.error)
                {
                    var error_html = '';
                    console.log(data.error); 
                }
                else 
                { 
                   console.log(data); 
                } 
            }
        })
});

 var count = 1;

 dynamic_field(count);

 function dynamic_field(number)
 {
  html = '<tr>';
        html += '<td><input type="text" name="dependent_name[]" placeholder="Enter Dependent Name" class="form-control name_list" required/></td><td><select name="relation[]" class="form-control" required><option value="">Select</option><option value="Father">Father</option><option value="Mother">Mother</option><option value="Son">Son</option><option value="Daughter">Daughter</option></select></td><td><select name="gender[]" class="form-control" required><option value="">Select</option><option value="Female">Female</option><option value="Male">Male</option></select></td><td><input type="date" name="dob[]" placeholder="Enter Dob" class="form-control name_list" required/></td><td><input type="text" name="age[]" placeholder="Enter Age" class="form-control name_list" required /></td><td><input type="text" name="adhar_number[]" placeholder="Enter Adhar" class="form-control name_list" required/></td><td><i class="remove material-icons">delete</i></td></tr>'; 
        if(number > 1)
        {
          //  html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></td></tr>';
            $('#user_table tbody').append(html);
        }
        else
        {   
            //html += '<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';
                        $('#user_table tbody').append(html);

            //$('tbody').html(html);
        }
 }

 $(document).on('click', '#add', function(){
  count++;
  dynamic_field(count);
 });

$(document).on('click', '#submitContactForm', function(){
   
 });

 $(document).on('click', '.remove', function(){
  count--;
  $(this).closest("tr").remove();
 });

 $('#dynamic_form').on('submit', function(event){
        event.preventDefault();
        $.ajax({
            url:'{{ route("sub-admin-divison.customers.addmember", $customer['id']) }}',
            method:'post',
            data:$(this).serialize(),
            dataType:'json',
            beforeSend:function(){
               // $('#save').attr('disabled','disabled');
            },
            success:function(data)
            {
                if(data.error)
                {
                    var error_html = '';
                    for(var count = 0; count < data.error.length; count++)
                    {
                        error_html += '<p>'+data.error[count]+'</p>';
                    }
                    $('#result').html('<div class="alert alert-danger">'+error_html+'</div>');
                }
                else 
                {
                   // dynamic_field(2);
                    $('#result').html('<div class="alert alert-success">'+data.success+'</div>');
setTimeout(location.reload.bind(location), 1000);

                }
              //  $('#save').attr('disabled', false);
            }
        })
 });

function submitContactForm(form_id)
{


}
});
</script>  @endsection