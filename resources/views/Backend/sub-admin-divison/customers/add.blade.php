@extends('backend/sub-admin-divison/common')

@section('content')
<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Add Customer</h4>
                  <p class="card-category">Complete the form</p>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('sub-admin-divison.customers.store')}}">
                    @csrf
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">State</label>
                          <select name="state" class="form-control" required>
                            <option value="">Please select one option</option>
                            @php $states = getStates() @endphp
                            @if(!empty($states))
                              @foreach($states as $value)
                               <option value="{{ $value->id }}"
                                @if ($value->id == '13')
                                  selected="selected"
                                @endif
                                >{{ $value->state }}</option>
                              @endforeach
                            @endif
                          </select>
                          @error('state')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">District</label>
                          <select name="district" class="form-control" required>
                            <option value="">Please select one option</option>
                            @php $districts = getDistricts()  @endphp
                            @if(!empty($districts))
                              @foreach($districts as $value)
                               <option value="{{ $value->id }}">{{ $value->district }}</option>
                              @endforeach
                            @endif
                          </select>
                          @error('district')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>                      
                      <div class="col-md-6">
                        <div class="form-group">
                        	                        <label class="bmd-label-floating">Country</label>

                          <label class="bmd-label-floating"></label>
                          <select name="district" class="form-control" required>
                               <option value="India">India</option>
                          </select> 
                        </div>
                      </div> 
                      <div class="col-md-6">
	                    <div class="form-group">
                          <label class="bmd-label-floating">Block name</label>
                          <input type="text" class="form-control" name="block" value="{{old('block')}}"  required>
                         @error('block')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
	                    <div class="form-group">
                          <label class="bmd-label-floating">Panchayat name</label>
                          <input type="text" class="form-control" name="panchayat" value="{{old('panchayat')}}"  required>
                         @error('panchayat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
	                    <div class="form-group">
                          <label class="bmd-label-floating">Village name</label>
                          <input type="text" class="form-control" name="village" value="{{old('village')}}"  required>
                         @error('village')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
	                    <div class="form-group">
                          <label class="bmd-label-floating">Rashancard Number</label>
                          <input type="text" class="form-control" name="rashancard_number" value="{{old('rashancard_number')}}"  required>
                         @error('rashancard_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
	                    <div class="form-group">
                          <label class="bmd-label-floating">Social Group</label>
                          <input type="text" class="form-control" name="rashancard_number" value="{{old('rashancard_number')}}"  required>
                         @error('rashancard_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
	                    <div class="form-group">
                          <label class="bmd-label-floating">Locality Address</label>
                          <input type="text" class="form-control" name="locality_address" value="{{old('locality_address')}}"  required>
                         @error('locality_address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
	                    <div class="form-group">
                          <label class="bmd-label-floating">Mobile Number</label>
                          <input type="text" class="form-control" name="mobile_number" value="{{old('mobile_number')}}"  required>
                         @error('mobile_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
	                    <div class="form-group">
                          <label class="bmd-label-floating">Rashancard Images</label>
                          <input type="file" class="form-control" name="rashancard_images"   >
                       
                        </div>
                      </div>

                      <div class="col-md-6">
	                    <div class="form-group">
                          <label class="bmd-label-floating">Category Proof Images</label>
                          <input type="file" class="form-control" name="category_proof"   >
                     
                        </div>
                      </div>

                    </div>

                     
                   
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
         
          </div>
        </div>
      </div>
  @endsection