<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]); // to restrict new user registration.

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

/******************** ADMIN ROUTES START *********************/

Route::group(['middleware' => ['auth', 'admin']], function () { 

     Route::get('/admin/dashboard', 'backend\admin\DashboardController@index')->name('admin.dashboard');
     Route::get('/admin/users', 'backend\admin\UserController@index')->name('admin.users');
     Route::get('/admin/users/add', 'backend\admin\UserController@add')->name('admin.users.add');
     Route::post('/admin/users/store', 'backend\admin\UserController@store')->name('admin.users.store');
     Route::get('/admin/users/edit/{id}', 'backend\admin\UserController@edit')->name('admin.users.edit');
     Route::post('/admin/users/update/{id}', 'backend\admin\UserController@update')->name('admin.users.update');
     Route::post('/admin/users/change-password', 'backend\admin\UserController@changePassword')->name('admin.users.change-password');
     Route::post('/admin/users/delete', 'backend\admin\UserController@delete')->name('admin.users.delete');
    
    Route::get('/admin/profile', 'backend\admin\CommonController@index')->name('admin.profile');
    Route::post('/admin/update-profile', 'backend\admin\CommonController@updateprofile')->name('admin.update-profile');

    Route::get('/admin/change-password', 'backend\admin\CommonController@changePassword')->name('admin.change-password');
    Route::post('/admin/update-password', 'backend\admin\CommonController@updatePassword')->name('admin.update-password');
});

/******************** ADMIN ROUTES END **************************/


/******************** SUB ADMIN ROUTES START **************************/

Route::group(['middleware' => ['auth', 'subadmin']], function () {

     Route::get('/sub-admin/dashboard', 'backend\subadmin\DashboardController@index')->name('sub-admin.dashboard');
     Route::get('/sub-admin/users', 'backend\subadmin\UserController@index')->name('sub-admin.users');
     Route::get('/sub-admin/users/add', 'backend\subadmin\UserController@add')->name('sub-admin.users.add');
     Route::post('/sub-admin/users/store', 'backend\subadmin\UserController@store')->name('sub-admin.users.store');
     Route::get('/sub-admin/users/edit/{id}', 'backend\subadmin\UserController@edit')->name('sub-admin.users.edit');
     Route::post('/sub-admin/users/update/{id}', 'backend\subadmin\UserController@update')->name('sub-admin.users.update');
     Route::post('/sub-admin/users/change-password', 'backend\subadmin\UserController@changePassword')->name('sub-admin.users.change-password');
     Route::post('/sub-admin/users/delete', 'backend\subadmin\UserController@delete')->name('sub-admin.users.delete');

      Route::get('/sub-admin/profile', 'backend\subadmin\CommonController@index')->name('sub-admin.profile');
      Route::post('/sub-admin/update-profile', 'backend\subadmin\CommonController@updateprofile')->name('sub-admin.update-profile');

     Route::get('/sub-admin/change-password', 'backend\subadmin\CommonController@changePassword')->name('sub-admin.change-password');
     Route::post('/sub-admin/update-password', 'backend\subadmin\CommonController@updatePassword')->name('sub-admin.update-password');

});


/******************** SUB ADMIN ROUTES END **************************/

/******************** SUB ADMIN ROUTES START **************************/


Route::group(['middleware' => ['auth', 'subadmindivison']], function () {

     Route::get('/sub-admin-divison/dashboard', 'backend\subadmindivison\DashboardController@index')->name('sub-admin-divison.dashboard');
     Route::get('/sub-admin-divison/users', 'backend\subadmindivison\UserController@index')->name('sub-admin-divison.users');
     Route::get('/sub-admin-divison/users/add', 'backend\subadmindivison\UserController@add')->name('sub-admin-divison.users.add');
     Route::post('/sub-admin-divison/users/store', 'backend\subadmindivison\UserController@store')->name('sub-admin-divison.users.store');
     Route::get('/sub-admin-divison/users/edit/{id}', 'backend\subadmindivison\UserController@edit')->name('sub-admin-divison.users.edit');
     Route::post('/sub-admin-divison/users/update/{id}', 'backend\subadmindivison\UserController@update')->name('sub-admin-divison.users.update');
    
     Route::post('/sub-admin-divison/users/change-password', 'backend\subadmindivison\UserController@changePassword')->name('sub-admin-divison.users.change-password');

     Route::post('/sub-admin-divison/users/delete', 'backend\subadmindivison\UserController@delete')->name('sub-admin-divison.users.delete');

     Route::get('/sub-admin-divison/profile', 'backend\subadmindivison\CommonController@index')->name('sub-admin-divison.profile');
      Route::post('/sub-admin-divison/update-profile', 'backend\subadmindivison\CommonController@updateprofile')->name('sub-admin-divison.update-profile');

     Route::get('/sub-admin-divison/change-password', 'backend\subadmindivison\CommonController@changePassword')->name('sub-admin-divison.change-password');
     Route::post('/sub-admin-divison/update-password', 'backend\subadmindivison\CommonController@updatePassword')->name('sub-admin-divison.update-password');

});

/******************** SUB ADMIN ROUTES END **************************/