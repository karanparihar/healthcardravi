-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2019 at 09:26 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `district` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `district`, `state_id`, `created_at`, `updated_at`) VALUES
(1, 'Bilaspur', 13, NULL, NULL),
(2, 'Chamba', 13, NULL, NULL),
(3, 'Hamirpur', 13, NULL, NULL),
(4, 'Kangra', 13, NULL, NULL),
(5, 'Kinnaur', 13, NULL, NULL),
(6, 'Kullu', 13, NULL, NULL),
(7, 'Lahaul and Spiti', 13, NULL, NULL),
(8, 'Mandi', 13, NULL, NULL),
(9, 'Shimla', 13, NULL, NULL),
(10, 'Sirmaur', 13, NULL, NULL),
(11, 'Solan', 13, NULL, NULL),
(12, 'Una', 13, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', NULL, NULL),
(2, 'Division Officer', 'Division Officer', NULL, NULL),
(3, 'Sub Division Officer', 'Sub Division Officer', NULL, NULL),
(4, 'Customer', 'Customer', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state`, `created_at`, `updated_at`) VALUES
(1, 'ANDAMAN AND NICOBAR ISLANDS', NULL, NULL),
(2, 'ANDHRA PRADESH', NULL, NULL),
(3, 'ARUNACHAL PRADESH', NULL, NULL),
(4, 'ASSAM', NULL, NULL),
(5, 'BIHAR', NULL, NULL),
(6, 'CHATTISGARH', NULL, NULL),
(7, 'CHANDIGARH', NULL, NULL),
(8, 'DAMAN AND DIU', NULL, NULL),
(9, 'DELHI', NULL, NULL),
(10, 'DADRA AND NAGAR HAVELI', NULL, NULL),
(11, 'GOA', NULL, NULL),
(12, 'GUJARAT', NULL, NULL),
(13, 'HIMACHAL PRADESH', NULL, NULL),
(14, 'HARYANA', NULL, NULL),
(15, 'JAMMU AND KASHMIR', NULL, NULL),
(16, 'JHARKHAND', NULL, NULL),
(17, 'KERALA', NULL, NULL),
(18, 'KARNATAKA', NULL, NULL),
(19, 'LAKSHADWEEP', NULL, NULL),
(20, 'MEGHALAYA', NULL, NULL),
(21, 'MAHARASHTRA', NULL, NULL),
(22, 'MANIPUR', NULL, NULL),
(23, 'MADHYA PRADESH', NULL, NULL),
(24, 'MIZORAM', NULL, NULL),
(25, 'NAGALAND', NULL, NULL),
(26, 'ORISSA', NULL, NULL),
(27, 'PUNJAB', NULL, NULL),
(28, 'PONDICHERRY', NULL, NULL),
(29, 'RAJASTHAN', NULL, NULL),
(30, 'SIKKIM', NULL, NULL),
(31, 'TAMIL NADU', NULL, NULL),
(32, 'TRIPURA', NULL, NULL),
(33, 'UTTARAKHAND', NULL, NULL),
(34, 'UTTAR PRADESH', NULL, NULL),
(35, 'WEST BENGAL', NULL, NULL),
(36, 'TELANGANA', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` int(11) NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT 1,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(11) DEFAULT NULL,
  `delete_flag` tinyint(4) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `parent`, `name`, `phone`, `district`, `state`, `email`, `role`, `email_verified_at`, `password`, `status_id`, `delete_flag`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 0, 'Himtech solutions', NULL, 0, NULL, 'admin@admin.com', 1, NULL, '$2y$10$W8EjU7akyb.r3fwc6lky1uumSy/uabCG.dgVBDnuFhxgZcy0E.c/O', NULL, 0, NULL, '2019-12-12 12:08:54', '2019-12-12 12:08:54'),
(4, 1, 'Karan1 Parihar1', NULL, 10, '15', 'karanparihar51@gmail.com', 2, NULL, '$2y$10$LVRgdJpYESQx7Zk6aYgcNekDCpBFY0DyMqfrMHhzKNg2dgbSN4cZ.', NULL, 0, NULL, NULL, '2019-12-13 14:55:55'),
(5, 1, 'Five seller', NULL, 6, '13', 'five@fiveexceptions.com', 3, NULL, '$2y$10$tr6oryDPmuUS09hu8Y7uzO7uofPQpFyU.k62.Ma0n./.zm.ZdMfoq', NULL, 0, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


